# STRAINS = [
#     "lodBeiA", "lodEloA", "lodEloB", "lodEloC", "lodEloD", "lodEloE", 
#     "lodEloF", "lodEloG", "lodEloH", "lodEloI", "lodEloJ", "lodEloK",
# ]

rule run:
    input: 
        "results/lodEloB_1.fna",
        "results/lodEloB_2.fna"

rule create_haplotyped_fasta:
    input:
        ref="data/{strain}.fa",
        vcf="cache/{strain}.phased.vcf.gz",
        tbi="cache/{strain}.phased.vcf.gz.tbi"
    output: "results/{strain}_{haplotype}.fna"
    shell: """
        bcftools consensus -H {wildcards.haplotype} -f {input.ref} {input.vcf} > {output}
    """

rule index:
    input: "cache/{strain}.phased.vcf.gz"
    output: "cache/{strain}.phased.vcf.gz.tbi"
    shell: "bcftools index -t {input}"

rule compress:
    input: "{file}"
    output: "{file}.gz"
    shell: "bgzip {input}"

rule hapcut2:
    input:
        bam="cache/{strain}_N.bam",
        vcf="cache/{strain}_I.biallel.vcf"
    output: "cache/{strain}.blocks", "cache/{strain}.phased.vcf"
    params: "cache/{strain}"
    shell: """
        extractHAIRS --bam {input.bam} --VCF {input.vcf} --out {params}.fragment
        HAPCUT2 \
            --fragments {params}.fragment \
            --VCF {input.vcf} \
            --output {output[0]} \
            --outvcf 1
        mv {output[0]}.phased.VCF {output[1]}
    """

rule remove_multiallel:
    input:
        vcf="cache/{strain}_I.vcf",
        ref="data/{strain}.fa"
    output: "cache/{strain}_I.biallel.vcf"
    params: "data/{strain}.dict"
    shell: """
        samtools dict {input.ref} -o {params}
        GenomeAnalysisTK                    \
            -T SelectVariants               \
            -R {input.ref}                  \
            -V {input.vcf}                  \
            -restrictAllelesTo BIALLELIC    \
            -o {output}
    """

rule long_bam:
    input:
        ref="data/{strain}.fa", 
        reads="data/{strain}_N.fastq.gz"
    output: "cache/{strain}_N.bam", "cache/{strain}_N.bam.bai"
    threads: workflow.cores
    shell: """
        minimap2 -ax map-ont --MD --eqx -t{threads} {input.ref} {input.reads}   \
        | samtools view -b                                                      \
        | samtools sort > {output[0]}

        samtools index {output[0]}
    """

rule short_vcf:
    input:
        ref="data/{strain}.fa",
        bam="cache/{strain}_I.bam"
    output: "cache/{strain}_I.vcf"
    shell: """
        freebayes                           \
            --bam {input.bam}               \
            --fasta-reference {input.ref}   \
            --vcf {output[0]}

        # --min-mapping-quality 50 --min-base-quality 20 --min-alternate-count 3
        # --min-alternate-fraction 0.5 --min-coverage 3 --pooled-continuous

        # gatk SelectVariants ???
    """

rule short_bam:
    input:
        multiext("data/{strain}.fa", ".amb", ".ann", ".bwt", ".pac", ".sa"),
        ref="data/{strain}.fa",
        reads1="data/{strain}_I1.fastq.gz",
        reads2="data/{strain}_I2.fastq.gz"
    output: "cache/{strain}_I.bam", "cache/{strain}_I.bam.bai"
    threads: workflow.cores
    shell: """
        bwa mem -t {threads} {input.ref} {input.reads1} {input.reads2}  \
        | samtools view -b                                              \
        | samtools sort > {output[0]}

        samtools index {output[0]}

        # picard MarkDuplicates ???
    """

rule bwa_index:
    input: "{reference}"
    output: multiext("{reference}", ".amb", ".ann", ".bwt", ".pac", ".sa")
    shell: """
        bwa index {input}
    """

# rule downsample:
# 	input: long_reads.bam
# 	output: downsampled_log_reads.bam
# 	params: coverage=29
# 	shell: """
#         /projects/haplotype-analysis/read_filtering/pruning-lodEloE.sh
#     """

