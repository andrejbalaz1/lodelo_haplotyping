# zcat lodEloB_I2.fastq.gz | paste - - - - | sort -k1,1 -S 3G | tr '\t' '\n' | gzip > sorted2.fq.gz
#         gzip {params[1]}
#         samtools collate -u -O in_pos.bam | \
#         | samtools collate \
#         | samtools fastq -1 {output[0]} -2 {output[1]}
# samtools view -h --fetch-pairs {input} {params[0]} > tmp.bam

rule:
    input: "cache_bup/lodEloB_I.bam",
    output: "data/lodEloB_I1.fastq.gz", "data/lodEloB_I2.fastq.gz"
    params: "chrH"
    shell: """
        samtools view -u -h -F 2304 --fetch-pairs {input} {params[0]} \
        | samtools collate -u -O - \
        | samtools fastq -n -1 {output[0]} -2 {output[1]} -s singletons.txt
        # singletons have unmapped pairs?
    """

# -F 2304 filters secondary and supplementary alignments, to avoid bug

# rule unmap_short:
#     input: "lodEloB_I.bam",
#     output: "lodEloB_I1.fastq.gz", "lodEloB_I2.fastq.gz"
#     shell: """
#         samtools collate -u -O {input} \
#         | samtools fastq -n -1 {output[0]} -2 {output[2]} -s single.txt
#     """
